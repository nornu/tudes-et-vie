(function($){
    var htmlNavig = $('header + .navig'),
        iTotal = 0,
        iItemTotal = 0,
        oItems = {};

    var windowInit = function(){
        if(window.innerWidth < 801){
            $('.cart').addClass('mobile');
            $('.cart ul').slideUp();
            htmlNavig.insertAfter('header');
        }else{
            $('.cart').removeClass('mobile');
            $('.cart ul').slideDown();
            htmlNavig.detach();
        }
        if(window.innerWidth < 1024){
            $('.level_2').addClass('.jsSlide').slideUp();
        }
    };

    var toggleMenu = function(e){
        e.preventDefault();
        var hSubmenu = e.currentTarget.nextElementSibling;
        $('.level_2').slideUp();
        if($(hSubmenu).hasClass('deployed')){
            $(hSubmenu).removeClass('deployed').slideUp();
        }else{
            $(hSubmenu).addClass('deployed').slideDown();  
        }
    };

    var showCart = function(e){
        var htmlCart = $(e.currentTarget),
            iTopEm = 3;
            iCartHeight = ((e.currentTarget.clientHeight)/-20)+iTopEm*2;
        if(htmlCart.hasClass('mobile')){
            htmlCart.find('ul').slideToggle();
            if(htmlCart.hasClass('cart_up')){
                htmlCart.removeClass('cart_up').addClass('cart_down');
            }else{
                htmlCart.removeClass('cart_down').addClass('cart_up');
            }
        }else if(!htmlCart.hasClass('front')){
            htmlCart.animate({'top':iCartHeight + 'em'},function(){
                htmlCart.animate({'z-index':20},10,function(){
                    htmlCart.animate({'top':iTopEm + 'em' }).addClass('front');
                });
            });    
        }else{
            htmlCart.animate({'top':iCartHeight + 'em'},function(){
                htmlCart.animate({'z-index':0},10,function(){
                    htmlCart.animate({'top':iTopEm + 'em'}).removeClass('front');
                });
            }); 
        }
        
    };

    var addToCart = function(e){
        e.preventDefault();
        var iPrice, sName;
        if(e.currentTarget.dataset.price){
            iPrice = (e.currentTarget.dataset.price).replace(',','.');
            iPrice = parseFloat(iPrice);
        }
        if(e.currentTarget.dataset.name){
            sName = e.currentTarget.dataset.name;
            console.log();
        }
        if(iPrice){
            iTotal += iPrice;
            if($('.cart ul li.empty')){
                $('.cart ul li.empty').remove();
            }
            $('.cart ul')
                .prepend('<li class="cart_item">- ' + sName + '</li>'+
                        '<li class="cart_price">€ ' + iPrice + '</li>');
        }
        if(localStorage.cart){
            var jCart = localStorage.cart;
            oCart = JSON.parse(jCart);
            iItemTotal = Object.keys(oCart).length;
        }
        iItemTotal += 1;
        $('#number_item').empty().append(iItemTotal);
        $('.total_price').empty().append('€ ' + iTotal);
    }

    var pushToLocalStorage = function(e){
        aItems = $('.cart ul').children();
        aTemp = aItems.slice(0,aItems.length-3);
        for (var i = 0; i < aTemp.length; i += 2) {
            var oTempItem = {};
            oTempItem.name = aTemp[i].innerText.replace('- ','');
            oTempItem.price = aTemp[i+1].innerText.replace('€ ','');
            oItems[i/2] = oTempItem;
        };
        json = JSON.stringify(oItems)
        localStorage.setItem('cart',json);
    }

    var autoFillCart = function(){
        var jCart = localStorage.cart;
        oCart = JSON.parse(jCart);
        oCart.length = Object.keys(oCart).length;
        $('.cart ul li.empty').remove();
        $('#number_item').empty().append(oCart.length)
        for (var i = 0; i < oCart.length; i++) {
            iTotal += parseFloat(oCart[i].price);
            $('.cart ul')
                .prepend('<li class="cart_item">- ' + oCart[i].name + '</li>'+
                    '<li class="cart_price">€ ' + oCart[i].price + '</li>');
        };
        $('.total_price').empty().append('€ ' + iTotal);
    }

    var displayCart = function(){
        var jCart = localStorage.cart,
        iTotal = 0;
        oCart = JSON.parse(jCart);
        oCart.length = Object.keys(oCart).length;
        if($('body').find('#sum_cart').length){
            $('#sum_cart article').hide().remove();
            for (var i = 0; i < oCart.length; i++) {
                sName = oCart[i].name;
                iTotal += parseFloat(oCart[i].price);
                $.ajax({
                    url : 'list_small.php',
                    type: 'POST',
                    data : {
                        product : sName
                    },
                    success:function(oResponse){
                        $('#sum_products').append(oResponse).slideDown();
                        $('#sum_cart .col_bottom.main_upper .price').empty().append('Total : € ' + iTotal);
                    },
                    error:function(oError){
                        console.log(oError)
                    }
                }) 
            }
        }
        
    }

    var initLocalStorage = function(){
        $('a[href*="index.php"]').on('click',pushToLocalStorage);
        $('a[href*="/"]').on('click',pushToLocalStorage);
        if(localStorage.cart){
            autoFillCart();
            displayCart();
        }
    }

    $(function(){
        windowInit();
        $(window).on('resize',windowInit);

        if(localStorage){
             initLocalStorage();
        }

        $('.cart').animate({'top':'3em'}).on('click',showCart);
        $('.level_11 a').on('click',toggleMenu);
        $('.add').on('click',addToCart);
        $('#everything').on('click',function(){
            localStorage.cart = "";
            initLocalStorage();
        })     
    })
}).call(this,jQuery);