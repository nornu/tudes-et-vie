    <a href="#top" class="navig">Remonter en haut de la page</a>
    <footer itemscope itemtype="http://schema.org/Organization">
        <div itemprop="description" class="description">
            <i>Une vaste gamme unique de produits spécialisés vous attend!</i>
            <p>
                Vous trouverez sur notre boutique en ligne des produits destinés au maintien et à l'amélioration de la qualité de la vie. Des technologies uniques de protections réellement mesurables et scientifiques, des instruments de mesure des champs électromagnétiques très performants, des blindages performants et des livres spécialisés. Ces produits uniques proviennent des quatre coins du globe et ont été soigneusement sélectionnés pour vous aider à mieux vivre dans un monde ou <i>l'electrosmog</i> est omniprésent.
            </p>
        </div>
        <div class="encarts">
            <div id="contact">
                <a href="index.php?page=contact">
                    <small itemprop="address">Rue du Bay-Bonnet, 32 <br>B-4620 Fléron <br>Belgique</small>
                    <small itemprop="telephone">00 32 (0) 4 355 17 84</small>
                    <small itemprop="email">info@etudesetvie.be</small>    
                </a>
            </div
            ><div id="livraison">
                <a href="index.php?page=cms">Livraison</a>
            </div
            ><div id="reservation">
                <a href="index.php?page=cms">Reservation</a>
            </div
            ><div id="cgdv">
                <a href="index.php?page=cms">Conditions générales de vente</a>
            </div
            ><div id="back">
                <a itemprop="url" href="http://etudesetvie.be"><span itemprop="name">Etudes et Vie</span> <small>le site web</small></a>
            </div>
        </div>
    </footer
    ><script src="js/jquery.js"></script>
    <script src="js/main.js"></script>
    <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='http://www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
</body>
</html>