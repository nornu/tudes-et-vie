<nav id="principal">
    <ul class="bar"><li class="lvl_0">
            <a href="./" title="Retourner à l'accueil d'études et vie">Accueil</a>
        </li><li class="lvl_0"
        >
            <a href="#" title="Voir les produits triés par utilisation">Par utilisation</a>
            <div class="lvl_1">
                <ul>
                    <li><a href="#">Se renseigner</a></li>
                    <li><a href="#">Se protéger</a></li>
                    <li><a href="#">Améliorer son confort de Vie</a></li>
                    <li><a href="#">Mesurer les nuisances</a></li>
                </ul>
            </div>
        </li><li class="lvl_0"
        >
            <a href="?" title="Voir les produits triés par marques">Par marques</a>
            <div class="lvl_1">
                <ul>
                    <li><a href="#">Elanra</a></li>
                    <li><a href="#">Antenne H3</a></li>
                    <li><a href="#">New-Daylite</a></li>
                    <li><a href="#">Philip Stein</a></li>
                    <li><a href="#">Prisma</a></li>
                    <li><a href="#">MAG-40</a></li>
                    <li><a href="#">Farbsonne</a></li>
                    <li><a href="#">Gigahertz-Solution</a></li>
                    <li><a href="#">Profi-Spion</a></li>
                    <li><a href="#">EMFields</a></li>
                    <li><a href="#">Smogtec</a></li>
                    <li><a href="#">ADR</a></li>
                    <li><a href="#">Danell</a></li>
                    <li><a href="#">PSO</a></li>
                    <li><a href="#">AlphaLab inc.</a></li>
                    <li><a href="#">Stetzerizer</a></li>
                    <li><a href="#">Geowave</a></li>
                    <li><a href="#">Isophone</a></li>
                    <li><a href="#">Envi</a></li>
                    <li><a href="#">POP Phone</a></li>
                    <li><a href="#">Biphysik Technology</a></li>
                    <li><a href="#">Global Mind</a></li>
                    <li><a href="#">Gamma Scout</a></li>
                    <li><a href="#">Ramon</a></li>
                    <li><a href="#">Yshield</a></li>
                </ul>
            </div>
        </li><li class="lvl_0"
        >
            <a href="?" title="Voir les produits triés par nuisances">Par nuisances</a>
            <div class="lvl_1">
                <ul class="col">
                    <li><a href="#">Basses-fréquences</a></li>
                    <li><a href="#">Hautes-fréquences</a></li>
                    <li><a href="#">Acoustiques</a></li>
                    <li><a href="#">Lumineuses</a></li>
                    <li><a href="#">Géomagnétiques</a></li>
                    <li><a href="#">Géobiologiques</a></li>
                    <li><a href="#">Champs électrostatiques</a></li>
                    <li><a href="#">Radioactivité</a></li>
                </ul>
            </div>
        </li><li class="lvl_0"
        >
            <a href="?" title="Voir tous les produits">Tous les produits</a>
            <div class="lvl_1">
                <ul class="col">
                    <li><a href="#">Lunettes</a></li>
                    <li><a href="#">Montres</a></li>
                    <li><a href="#">Vêtements</a></li>
                    <li><a href="#">Baldaquins</a></li>
                    <li><a href="#">Tapis de sol</a></li>
                    <li><a href="#">Linge de lit</a></li>
                    <li><a href="#">Protection pour DECT</a></li>
                    <li><a href="#">Housses&nbsp;pour telephones&nbsp;portables</a></li>
                    <li><a href="#">Ecouteurs</a></li>
                    <li><a href="#">Geowave</a></li>
                    <li><a href="#">Four à micro-ondes</a></li>
                    <li><a href="#">Acoustimètre</a></li>
                    <li class="secondary"><a href="#">Accessoires Acoustimètres</a></li>
                    <li><a href="#">Analyseur de Basse fréquences</a></li>
                        <li class="secondary"><a href="#">Accessoires pour analyseurs de basses fréquences</a></li>
                        <li class="secondary"><a href="#">Kit d'analyseur de basses fréquences</a></li>
                    <li><a href="#">Analyseur de Hautes fréquences</a></li>
                        <li class="secondary"><a href="#">Accessoires pour analyseurs de Hautes fréquences</a></li>
                        <li class="secondary"><a href="#">Kit d'analyseur de Hautes fréquences</a></li>
                    <li><a href="#">Antennes de détéction radiesthésique</a></li>
                        <li class="secondary"><a href="#">Accessoires pour antennes de détéctioin radiesthésiques</a></li>
                    <li><a href="#">Sonde éléctrostatiques</a></li>
                    <li><a href="#">Volmètres de surface</a></li>
                    <li><a href="#">Géomagnétomètres</a></li>
                        <li class="secondary"><a href="#">Accessoires pour Géomagnétomètres</a></li>
                    <li><a href="#">Compteurs d'ions</a></li>
                        <li class="secondary"><a href="#">Accessoires Compteurs d'ions</a></li>
                    <li><a href="#">Sondes photoélectriques</a></li>
                    <li><a href="#">Détecteur de pollution lumineuse</a></li>
                    <li><a href="#">Compteur Geiger</a></li>
                        <li class="secondary"><a href="#">Accessoires de Radioactivité</a></li>
                    <li><a href="#">Moniteur de radon</a></li>
                    <li><a href="#">Ionisateur</a></li>
                        <li class="secondary"><a href="#">Accessoires pour ionisateurs</a></li>
                    <li><a href="#">Multiprises</a></li>
                    <li><a href="#">Cablages</a></li>
                    <li><a href="#">Prises et adaptateurs</a></li>
                    <li><a href="#">Filtres électriques</a></li>
                    <li><a href="#">Compensateur CAP</a></li>
                    <li><a href="#">Livres</a></li>
                    <li><a href="#">DVD</a></li>
                    <li><a href="#">Éclairages</a></li>
                    <li><a href="#">Pendules et énergies micro-vibratoires</a></li>
                    <li><a href="#">Peintures de blindages</a></li>
                        <li class="secondary"><a href="#">Accessoires pour peinturesde blindage</a></li>
                    <li><a href="#">Toiles de blindages</a></li>
                        <li class="secondary"><a href="#">Accessoires pour toiles de blindages</a></li>
                </ul>
            </div>
        </li><li class="lvl_0"
        >
            <form action="#">
                <label id="searchlab" for="search">Rechercher sur le site</label>
                <input type="text" id="search" name="search" placeholder="Rechercher sur le site...">
                <input type="submit" value="Rechercher">
            </form>
        </li></ul>
</nav>