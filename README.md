## Projet de refonte d'un e-commerce

Projet pour le cours de Projet Web 2013-2014

####Objectifs du projet
- Prise en main du local storage

####Consignes du projet
- Refonte d'un e-commerce
- Maquette en local storage

[ecommerce.jcheron.be](ecommerce.cheron.be)

*Haute École de la Province de Liège*