<div id="navigation">
        <h1 class="doc_plan">
            Navigation principale
        </h1>
        <div id="logos" class="dropdown level_0">
            <ul>
                <li class="level_01">
                    <a href="./"><span id="home"></span></a>
                </li>
                <li class="level_01">
                    <a href="#"><span id="action"></span></a>
                </li>
                <li class="level_01">
                    <a href="#"><span id="dangers"></span></a>
                </li>
                <li class="level_01">
                    <a href="#"><span id="tag"></span></a>
                </li>
                <li class="level_01">
                    <a href="?"><span id="everything"></span></a>
                </li>
                <div id="intitules" class="dropdown level_1">
                    <ul>
                        <li class="level_11"><a href="./">Accueil</a></li>
                        <li class="level_11">
                            <a href="#">Nous contacter</a>
                            <div id="sub_cat_1" class="dropdown level_2">
                                <ul>
                                    <li>Infos de contact</li>
                                </ul>
                            </div>
                        </li>
                        <li class="level_11">
                            <a href="#">Les nuisances <small>Informations</small></a>
                            <div id="sub_cat_2" class="dropdown level_2">
                                <ul class="col">
                                    <li><a href="#">Basses-fréquences</a></li>
                                    <li><a href="#">Hautes-fréquences</a></li>
                                    <li><a href="#">Acoustiques</a></li>
                                    <li><a href="#">Lumineuses</a></li>
                                    <li><a href="#">Géomagnétiques</a></li>
                                    <li><a href="#">Géobiologiques</a></li>
                                    <li><a href="#">Champs électrostatiques</a></li>
                                    <li><a href="#">Radioactivité</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="level_11">
                            <a href="#">Études &amp; vie <small>le label</small></a>
                            <div id="sub_cat_3" class="dropdown level_2">
                                <ul>
                                    <li>Informations sur le label études &amp; vie</li>
                                </ul>
                            </div>
                        </li>
                        <li class="level_11">
                            <a href="#">Reset du panier</a>
                            <div id="sub_cat_4" class="dropdown level_2">
                                <ul class="col">
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </ul>
        </div>
        <div class="overlay"></div>
    </div>