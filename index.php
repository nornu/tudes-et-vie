<?php
if ($handle = opendir('img/')) {
    while (false !== ($entry = readdir($handle))) {
        if(!($entry == '.' || $entry == '..')){
           $temp_paths[] = $entry; 
        }
    }
}
foreach ($temp_paths as $path) {
    $paths[] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $path);
}
$paths[]='montre';
$template_paths = array('cart','step1','login','cms','contact');
if(isset($_GET['page']) && in_array($_GET['page'], $template_paths)){
    $page = $_GET['page'];
}elseif(isset($_GET['page']) && in_array($_GET['page'], $paths) ){
    $page = 'produit';
}else{
    $page = 'home';
}
include('header.php');
include('navmenu.php');
?>
<div class="login">
    <a id="subscribe" href="index.php?page=login" class="button">S'inscrire</a>
    <a id="connect" href="index.php?page=login" class="button">S'identifier</a>
    <div class="hover">
        <form action="index.php">
            <label for="login">Identifiant</label>
            <input type="text" name="login" id="login">
            <label for="pwd">Mot de passe</label>
            <input type="password" name="pwd" id="pwd">
            <input type="submit" value="S'identifier">
        </form>
    </div>
</div>
<div class="content">
    <?php include($page.'.php'); ?>
</div>

<?php 
include('navsecondaire.php');
include('footer.php');
?>
