<?php include('products.php');
$product = $products[array_rand($products)];
$image = $product['image'];
$url = preg_replace('/\\.[^.\\s]{3,4}$/', '', $image);?>
<article itemscope itemtype="http://schema.org/Product" >
    <a itemprop="url" href="index.php?page=<?php echo $url ?>" title="Aller vers la page de <?php echo $product['name'];?>">
        <h1 itemprop="name">
                <span itemprop="manufacturer"><?php echo $product['manufacturer'];?></span> <?php echo $product['name'];?>
        </h1>
        <figure>
            <img itemprop="image" src="img/<?php echo $image;?>" alt="Photo du <?php echo $product['name'];?>">
            <figcaption>
                <p itemprop="legend"><?php echo $product['legend'];?></p>
            </figcaption>
        </figure>
        <p itemprop="description">
            <?php echo $product['description'];?>
        </p>
        <p itemprop="price" class="price">€ <?php echo $product['price'];?> <small>TTC</small></p>
    </a>
    <a href="javascript:void(0)" class="add" data-name="<?php echo $product['legend'];?>" data-price="<?php echo $product['price'];?>">Ajouter au panier</a>
    
</article>