<?php 

/********************************************************

    array(
        'name'=>'',
        'manufacturer'=>'',
        'image'=>'',
        'legend'=>'',
        'description'=>'',
        'price'=>'',
        ),

********************************************************/

$products = array(
    array(
        'name'=>'Montre',
        'manufacturer'=>'Philip Stein',
        'image'=>'montre.jpg',
        'legend'=>'Montre Philip Stein models Prestige',
        'description'=>'Les montres de la collection "Prestige" de PHILIP STEIN produisent des fréquences bénéfiques pour la vie et la santé identiques à celles émises par la Terre. Elles sont modernes, très luxuseuses et de de manufacture horlogère Suisse. Elles existent avec des cadrans larges, small, mini pour vos cocktails et de style chronographes.',
        'price'=>'1510,05',
        ),
    array(
        'name'=>'BIORUPTEUR ®',
        'manufacturer'=>'PSO',
        'image'=>'bio.jpg',
        'views'=>array('bio.jpg','bio.jpg'),
        'legend'=>'BIORUPTEUR ®',
        'description'=>'La pollution électrique 50 Hz est celle que l’on rencontre le plus dans nos pièces de repos et dans les chambres à coucher. Un fois identifié, le rayonnement électrique qui se diffuse peut-être supprimé sans difficulté avec le Biorupteur ®.',
        'price'=>'150,00',
        ),
    array(
        'name'=>'Voile écran -40dB pro blanc intense',
        'manufacturer'=>'Études & vie',
        'image'=>'voile.jpg',
        'legend'=>'Voile écran -40dB pro blanc intense',
        'description'=>'Exceptionnel! Enfin un voile de couleur blanc intense et transparent de 3 m de large et de protection élevée contre les hautes fréquences pour fabriquer vos rideaux et baldaquins',
        'price'=>'99,00',
        ),
    array(
        'name'=>'Casquette écran pour dame',
        'manufacturer'=>'Körper',
        'image'=>'casquette.jpg',
        'legend'=>'Casquette écran pour dame',
        'description'=>'Casquette écran pour damme blindée contre les hautes fréquences. ',
        'price'=>'65,00',
        ),
    array(
        'name'=>'Géomagnétomètre BPT 2010 - professionnel',
        'manufacturer'=>'Phisik Technologic',
        'image'=>'geo.jpg',
        'legend'=>'Géomagnétomètre BPT 2010 - professionnel',
        'description'=>'Le géomagnétomètre BPT2010 mis au poit initialement par le chercheur de génie Ludger Mersmann (Ing.) mesure les variations (gradients) et restitue un graphique 3D de la composante verticale du champ magnétique terrestre topographique.',
        'price'=>'2398,27',
        ),
    array(
        'name'=>'DK90L colle de dispersion conductrice pour toile de blindage',
        'manufacturer'=>'Y-Shield',
        'image'=>'colle.jpg',
        'legend'=>'DK90L colle de dispersion conductrice pour toile de blindage',
        'description'=>'Nouvelle colle de dispersion DK90L conductrice destinée à coller les toiles de blindage (HNG80, HNV80...) sur le sol, les murs, les portes et les plafonds.',
        'price'=>'71,07',
        ),
    array(
        'name'=>'Sonde de scintillement gamma avec un cristal de NaI',
        'manufacturer'=>'Physik Technologic',
        'image'=>'sonde.jpg',
        'legend'=>'Sonde de scintillement gamma avec un cristal de NaI',
        'description'=>'La sonde de scintillation gamma est composée d\'un cristal d\'iodure de sodium (NAI). C\'est un accessoire du Géoscanner BPT3010 qui mesure les variations topographiques et spectrométrique de la radioactivité gamma (KeV) d\'origine tellurique en 2D et 3D.',
        'price'=>'5801,00',
        ),
    array(
        'name'=>'MATELAS ANTI-ONDES ADR-MAT',
        'manufacturer'=>'ADR',
        'image'=>'adrmat.jpg',
        'legend'=>'MATELAS ANTI-ONDES ADR-MAT',
        'description'=>'Le matelas ADR MAT® est un matelas qui protège le corps du dormeur pendant son sommeil contre les champs électriques de basses fréquences provenant du sol et des apapreils électriques présents sous la chambre ou sous le lit. Découvrez les trois tailles : lit bébé et enfant, lit d\'une personnes (L) et lit de deux personnes (XL).',
        'price'=>'58,00',
        ),
    array(
        'name'=>'MAG 40 - APPAREIL MÉDICAL',
        'manufacturer'=>'Luxomed',
        'image'=>'mag40.jpg',
        'legend'=>'MAG 40 - APPAREIL MÉDICAL',
        'description'=>'Le MAG 40® est un générateur de champs magnétiques pulsés pré-programmable. Il est équipé de mémoires, et se règle sur plusieurs fréquences à utiliser l’une à la suite de l’autre, au cours d’un même traitement.',
        'price'=>'975,27',
        ),
    array(
        'name'=>'MK30 KIT DE MESURE SEMI-PRO DE L\'ÉLECTROSMOG',
        'manufacturer'=>'Gigahertz Solution',
        'image'=>'kit.jpg',
        'legend'=>'MK30 KIT DE MESURE SEMI-PRO DE L\'ÉLECTROSMOG',
        'description'=>'Ce kit MK 30 est la combinaison parfaite entre l\'amateur et le technicien professionnel. Contenu du kit : HF38B + ME3840B incl. accessoires dans une valise en mousse et en plastique solide.',
        'price'=>'798,22',
        ),
    array(
        'name'=>'Stimulateur',
        'manufacturer'=>'ADR-4',
        'image'=>'adr.jpg',
        'legend'=>'ADR-4 Stimulateur',
        'description'=>'Le dispositif ADR-4 est un apareil utilisé pour éméliorer les propriétés physiques et la structure de l\'eau alimentaire de même que l\'eau qui est contenue dans les aliments et les boissons. Ces améliorations sont bénéfiques pour notre santé. Il permet aussi d\'apporter certains soins à notre organisme.',
        'price'=>'59,00',
        ),
    array(
        'name'=>'Oreillette A5 STEREO',
        'manufacturer'=>'AIRCOM',
        'image'=>'oreillette.jpg',
        'legend'=>'Oreillette AIRCOM-A5 STEREO',
        'description'=>'Nouveau! Cette oreillette AIRCOM A5 réduit à 99 % la conduction des basses et des hautes fréquences dans le fil du kit mains libres de votre portables-GSM. Elle est livrée d\'origine avec une prise jack 3.5 mm pour iphone et d\'autres marques récentes (Samsung, LG, Blackberry...). Pour les autres marques et modèles, consultez la liste des adaptateurs compatibles avec votre portable (GSM).',
        'price'=>'69,95',
        ),
    );