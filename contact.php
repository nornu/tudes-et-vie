<div class="main_upper">
    <div class="center">
        <div class="col_bottom">
            <form action="#">
                <label for="name">Votre nom et prénom</label>
                <input type="text" name="name">
                <label for="mail">Votre adresse e-mail</label>
                <input type="text" name="mail">
                <label for="subject">Le sujet de votre message</label>
                <input type="text" name="subject">
                <label for="content">Le message</label>
                <textarea name="content" rows="8" placeholder="Votre message ici..."></textarea>
                <input type="submit" value="Envoyer">
            </form>
        </div>
    </div>
</div>