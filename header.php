<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Études & Vie</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <?php if(isset($_GET['page']) && $_GET['page'] == ('cart' || 'produit')):?>
        <link rel="stylesheet" href="css/produit.css">
    <?php endif; ?>

    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <header id="top">
        <h1>
            <a href="./">
                <div class="main_title">
                    Études <span id="ampersand">&amp;</span><span id="vie">Vie</span>
                </div>
            </a>
        </h1>
        <div class="cart cart_down">
            <h2>
                <a href="index.php?page=cart" title="Voir le panier">
                    <span id="cart_icon">
                        <span id="number_item">0</span>
                    </span>
                        Panier
                </a>    
            </h2>
            
            <ul>
                <li class="empty">(Vide)</li>
                <li class="divider"></li>
                <li class="cart_item">Total</li>
                <li class="cart_price total_price">€ 00,00</li>
            </ul>
        </div>
    </header>
    <a href="#navigation" class="navig">Aller à la navigation</a>