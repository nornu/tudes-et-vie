<?php
if(!isset($product)){
    $product = array(
        'name'=>'Montre',
        'manufacturer'=>'Philip Stein',
        'image'=>'montre.jpg',
        'legend'=>'Montre Philip Stein models Prestige',
        'description'=>'Les montres de la collection "Prestige" de PHILIP STEIN produisent des fréquences bénéfiques pour la vie et la santé identiques à celles émises par la Terre. Elles sont modernes, très luxuseuses et de de manufacture horlogère Suisse. Elles existent avec des cadrans larges, small, mini pour vos cocktails et de style chronographes.',
        'price'=>'1510,05',
        );
}
$image = $product['image'];
$url = preg_replace('/\\.[^.\\s]{3,4}$/', '', $image)
?>
<article itemscope itemtype="http://schema.org/Product" class="splash">
    <div class="main_product">
        <h1 itemprop="name">
            <a itemprop="url" href="index.php?page=<?php echo $url ?>" title="Aller vers la page Philip Stein Montre">
                <?php echo $product['name'] ?> <span itemprop="manufacturer"><?php echo $product['manufacturer'] ?></span>
            </a>
        </h1>
        <p itemprop="description">
            <?php echo $product['description'] ?>
        </p>
        <a href="index.php?page=<?php echo $url ?>" class="button">Voir plus de détails</a>
        <div class="buttons_group">
            <p itemprop="price" class="price">€ <?php echo $product['price'] ?> <small>TTC</small></p>
            <a href="#" class="add" data-name="<?php echo $product['legend'] ?>" data-price="<?php echo $product['price'];?>">Ajouter au panier</a>    
        </div>
        
    </div>
    <a href="index.php?page=<?php echo $url ?>" title="Aller vers la page Philip Stein Montre">
        <figure>
            <img itemprop="image" src="img/<?php echo $image ?>" alt="Photo de <?php echo $product['name'] ?>">
            <figcaption itemprop="legend">
                <p><?php echo $product['legend'] ?>"</p>
            </figcaption>
        </figure>
    </a>
</article>