<?php
include('products.php');
$needle = $_GET['page'].'.jpg';
foreach ($products as $product_temp) {
    if(array_search($needle, $product_temp)){
        $product = $product_temp;
    }
}
?>
<div class="main_upper">
    <?php include('splash.php') ?>
</div>
<div class="center">
    <?php if(isset($product['views'])):?>
    <div class="gallery">
        <h2>Autres vues du produit</h2>
        <?php foreach ($product['views'] as $view):?>
        <a class="zoom" href="#" title="Agrandir l'image">
            <figure>
                <img src="img/<?php echo $view ?>" alt="Photo de <?php echo $product['name'] ?>">
                <figcaption><small><?php echo $product['legend'] ?></small></figcaption>
            </figure>
        </a>
        <?php endforeach;?>
    </div>
    <?php endif;?>
    <article class="caracteristics">
        <h2>Description détaillée</h2>
        <p><?php echo $product['description'] ?></p>
        <aside>
            <ul>
                <li>Nom : <?php echo $product['name'] ?></li>
                <li>Marque : <?php echo $product['manufacturer'] ?></li>
                <li>Autres infos : <?php echo $product['legend'] ?></li>
            </ul>    
        </aside>
    </article>
    <div class="main_upper">
        <h2>Accessoires</h2>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
    </div>
    <div class="main_upper">
        <h2>Produits similaires</h2>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
    </div>
    <div class="main_upper">
        <h2>Produits consultés récement</h2>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
        <?php include('list_small.php') ?>
    </div>
</div>